<!DOCTYPE HTML>
<html>
    <head>
        <?php include 'common/common.php' ?>
        <script src="js/platos.js" type="text/javascript"></script>
        <link href="css/platos.css" rel="stylesheet" type="text/css"/>
    </head>    
    <body>
        <a href="index.php" style="float: left">Volver</a>

        <?php include 'common/renders/header.php' ?>

        <h3>Platos</h3>
        <div>
            <span>Ordenar por</span>
            <select id="lista">
                <option value="codigo">Codigo</option>
                <option value="nombre" >Nombre (A-Z)</option>
                <option value="-nombre">Nombre (Z-A)</option>
                <option value="precio">Precio (bajo a alto)</option>
                <option value="-precio">Precio (alto a bajo)</option>
            </select>
        </div>

        <br/>

        <table id="tabla"></table>


        <?php include 'common/renders/footer.php' ?>
    </body>
</html>
