<!-- Titulo -->
<title><?php echo TITLE ?></title>

<!-- Otros -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- JavaScript -->
<script src="common/js/jquery.js" type="text/javascript"></script>
<script src="common/js/header.js" type="text/javascript"></script>
<script src="common/js/footer.js" type="text/javascript"></script>

<!-- Styles -->
<link href="common/css/style.css" rel="stylesheet" type="text/css"/>
<link href="common/css/header.css" rel="stylesheet" type="text/css"/>
<link href="common/css/footer.css" rel="stylesheet" type="text/css"/>