<!DOCTYPE HTML>
<html>
    <head>        
        <?php include 'common/common.php' ?>
        <script src="js/index.js" type="text/javascript"></script>
        <link href="css/index.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php include 'common/renders/header.php' ?>

        <h3>Plato</h3>
        <div>
            <select name="plato" id="lista">
                <option>Cargando lista...</option>
            </select>
            <input type="button" id="boton" value="Ver precio"/>
            <br/>
            <div id="precio">&nbsp;</div>
            <br/>
            <a href="platos.php">Ver todos</a>
            <div id="comentarios"></div>
        </div>

        <?php include 'common/renders/footer.php' ?>
    </body>
</html>