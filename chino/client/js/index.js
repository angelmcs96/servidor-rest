$(function () {
    $.ajax({
        url: "http://localhost/chino/chino/platos",
        dataType: 'JSON',
        //data: {cod: $('#carrera').val()},
        type: "GET",
        success: function (data) {
            platos = data.platos;
            llenarLista();
        },
        error: function (error) {
            $("#comentarios").text("Error: " + error.status + " " + error.statusText);
        }
    });
    $("#boton").click(insertarPrecio);
    $("#lista").change(insertarPrecio);
});

function llenarLista() {
    $("#lista").text("");

    platos.forEach(function (plato) {
        $("#lista").append(`<option>${plato.nombre}</option>`);
    });
}

function insertarPrecio() {
    var nombre = $("#lista option:selected").text();

    var plato = platos.find(function (plato) {
        return plato.nombre === nombre;
    });

    $("#precio").html(`${plato.precio} &euro;`);
}

var platos = [];
