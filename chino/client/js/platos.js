$(function () {
    pedirLista();
    $("#lista").change(pedirLista);
});

function pedirLista() {
    var orden = $("#lista option:selected").val();

    var url = `http://localhost/chino/chino/platos/order/${orden}`;

    // console.log(url);

    $.ajax({
        url: url,
        dataType: 'JSON',
        //data: {clave: valor},
        type: "GET",
        success: llenarTabla,
        error: function (error) {
            $("#comentarios").text("Error: " + error.status + " " + error.statusText);
        }
    });
}

function llenarTabla(datosJSON) {
    var platos = datosJSON.platos;

    // Eliminar elementos anteriores
    $('#tabla').html('');

    for (var i = 0; i < platos.length; i++) {
        var fila, columnaNombre, columnaPrecio;

        fila = document.createElement('tr');
        fila.style.backgroundColor = (i % 2 === 0) ? 'lightblue' : 'lightgreen';

        columnaNombre = document.createElement('td');
        columnaNombre.colspan = 2;
        columnaNombre.appendChild(document.createTextNode(platos[i].nombre));

        columnaPrecio = document.createElement('td');
        columnaPrecio.style.textAlign = 'right';
        columnaPrecio.style.paddingLeft = '1em';
        columnaPrecio.appendChild(document.createTextNode(`${platos[i].precio} €`));


        fila.appendChild(columnaNombre);
        fila.appendChild(columnaPrecio);

        $("#tabla").append(fila);
    }
}
