-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-07-2018 a las 19:42:17
-- Versión del servidor: 10.1.22-MariaDB
-- Versión de PHP: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `chino`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `platos`
--

CREATE TABLE `platos` (
  `codigo` varchar(3) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `precio` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `platos`
--

INSERT INTO `platos` (`codigo`, `nombre`, `precio`) VALUES
('000', 'El plato no existe', '0'),
('001', 'Ensalada china', '3.15'),
('002', 'Ensalada con gambas', '3.60'),
('003', 'Ensalada de la casa', '4.30'),
('004', 'Ensalada con brotes de soja', '2.80'),
('005', 'Rollo de primavera', '1.40'),
('006', 'Wan-Tun frito', '2.90'),
('007', 'Pan de gambas', '2.10'),
('008', 'Entremeses', '5.35'),
('010', 'Verdura frita', '3.70'),
('011', 'Sopa de aletas de tiburón', '3.05'),
('012', 'Sopa agripicante', '2.45'),
('013', 'Sopa de fideos chinos con pollo y Tsa-Chai(picante)', '2.65'),
('014', 'Sopa de Wan-Tun', '3.00'),
('015', 'Sopa de pollo con champiñones', '2.55'),
('016', 'Sopa de pollo con maiz', '2.55'),
('017', 'Sopa de cangrejo con maiz', '2.55'),
('018', 'Sopa de verdura', '2.15'),
('019', 'Sopa de pollo', '2.40'),
('020', 'Sopa de marisco', '3.30'),
('021', 'Arroz frito con tres delicias', '3.45'),
('022', 'Arroz frito con gambas', '4.00'),
('023', 'Arroz frito de Gran Muralla', '4.55'),
('024', 'Arroz blanco', '2.15'),
('025', 'Arroz con pollo y curry', '3.95'),
('026', 'Arroz con ternera y curry', '3.95'),
('027', 'Arroz con ocho delicias', '4.15'),
('028', 'Tallarines fritos con tres delicias', '4.20'),
('029', 'Tallarines fritos con gambas', '4.40'),
('030', 'Tallarines fritos con pollo o ternera', '4.05'),
('031', 'Fideos chinos tres delicias', '4.55'),
('032', 'Fideos singapur', '4.80'),
('033', 'Fideos de soja fritos con tres delicias', '5.20'),
('034', 'Tallarines chinos a la plancha', '5.25'),
('035', 'Patatas fritas', '2.15'),
('036', 'Pan chino', '1.35'),
('037', 'Huevos revueltos con gambas', '3.65'),
('038', 'Huevos revueltos con jamón', '3.45'),
('039', 'Huevos revueltos con pollo', '3.45'),
('040', 'Huevos con patatas', '3.45'),
('041', 'Verduras variadas salteadas', '3.15'),
('042', 'Bambús y setas chinas salteadas', '4.50'),
('043', 'Brotes de soja salteados', '3.45'),
('044', 'Champiñones salteados', '4.20'),
('045', 'Pescado con bambú y setas chinas', '4.85'),
('046', 'Pescado frito con picante', '4.75'),
('047', 'Pescado agridulce', '4.60'),
('048', 'Calamares fritos', '5.15'),
('049', 'Calamares con salsa picante', '5.70'),
('04a', 'Ensalada de brote de soja con gambas', '4.20'),
('050', 'Gambas a la plancha', '6.95'),
('051', 'Gambas con verduras', '5.15'),
('052', 'Gambas con bambú y setas chinas plancha', '6.80'),
('053', 'Gambas con brotes de soja', '5.15'),
('054', 'Gambas con salsa de curry', '6.25'),
('055', 'Gambas con salsa picante', '6.20'),
('056', 'Gambas con champiñón', '6.20'),
('057', 'Gambas con salsa agridulce', '5.80'),
('058', 'Langostinos fritos', '5.80'),
('059', 'Gambas con salsa de ostras', '6.15'),
('05a', 'Rollitos estilo Vietnam(4)', '3.50'),
('060', 'Pollo con salsa de alubias y patatas fritas', '4.60'),
('061', 'Pollo con verduras', '4.25'),
('062', 'Pollo con bambú y setas chinas', '5.25'),
('063', 'Pollo con brotes de soja', '4.25'),
('064', 'Pollo con salsa curry', '4.35'),
('065', 'Pollo con salsa picante', '4.35'),
('066', 'Pollo con champiñón', '4.95'),
('067', 'Pollo agridulce', '4.65'),
('068', 'Pollo al limón', '4.75'),
('069', 'Pollo con piña', '4.50'),
('070', 'Pollo con almendras', '4.70'),
('071', 'Ternera con verduras', '4.50'),
('072', 'Ternera con bambú y setas chinas', '5.60'),
('073', 'Ternera con brotes de soja', '4.45'),
('074', 'Ternera con salsa curry', '4.55'),
('075', 'Ternera con salsa picante', '4.55'),
('076', 'Ternera con champiñones', '5.20'),
('077', 'Ternera con pimientos verdes', '4.50'),
('078', 'Ternera con cebollas', '4.50'),
('079', 'Ternera con salsa de ostras', '5.05'),
('080', 'Ternera a la plancha', '6.60'),
('081', 'Cerdo con verduras', '4.45'),
('082', 'Cerdo agridulce', '4.35'),
('083', 'Cerdo con brotes de soja', '4.35'),
('084', 'Cerdo con salsa curry', '4.55'),
('085', 'Cerdo con salsa picante', '4.55'),
('086', 'Cerdo con champiñon', '4.70'),
('087', 'Costillas asadas', '5.45'),
('088', 'Costillas agridulces', '5.15'),
('090', 'Cerdo con bambú y setas chinas', '5.15'),
('091', 'Pato asado estilo Pekín', '9.50'),
('092', 'Pato con bambú y setas chinas', '7.40'),
('093', 'Pato con piña', '7.20'),
('094', 'Pato a la naranja', '7.50'),
('095', 'Pato con salsa picante', '7.50'),
('096', 'Pato al limón', '7.40'),
('097', 'Kubak con tres delicias', '5.80'),
('098', 'Kubak con gambas', '6.00'),
('099', 'Familia feliz (mariscos y carnes salteadas)', '6.35'),
('100', 'Bolitas de pollo fritas', '4.30'),
('101', 'Pollo frito con patatas', '4.80'),
('102', 'Pollo Hong Kong', '7.10'),
('103', 'Ternera con gambas y Tsu-Chai (picante)', '6.85'),
('104', 'Hormigas subir al árbol', '6.05'),
('105', 'Pollo a la Tie-Ban', '5.90'),
('107', 'Mariscos variados Tie-Ban', '6.25'),
('108', 'Ancas de rana a la plancha', '6.95'),
('109', 'Ancas de rana con pimienta y sal', '6.90'),
('110', 'Pollo Kon-Pao', '5.80'),
('112', 'Berenjenas con salsa \"Xu Xiang\"', '4.00'),
('113', 'Filete de pollo con patatas', '4.70'),
('114', 'Gambas con pimienta y sal', '6.60');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `platos`
--
ALTER TABLE `platos`
  ADD PRIMARY KEY (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
