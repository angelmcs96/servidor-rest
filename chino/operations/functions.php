<?php

/**
 * Se usa para comprobar si una serie de valores son numericos
 * */
function are_numbers($array) {
    foreach ($array as $valor) {
        if (!is_numeric($valor)) {
            return false;
        }
    }
    return true;
}

/**
 * Se usa para comprobar si un array tiene valores vacios
 */
function array_has_null_elements($array) {
    $nulos = array();
    foreach ($array as $clave => $valor) {
        if (is_null($valor) || empty(trim($valor))) {
            $nulos[] = $clave;
        }
    }
    return $nulos;
}

/**
 * Se usa para comprobar si un array asociativo contiene una serie de valores
 * y devuelve los que no existan
 */
function null_elements_of_array($array, $elementos) {
    $nulos = array();
    foreach ($elementos as $elemento) {
        if (!isset($array[$elemento]) || is_null($array[$elemento])) {
            $nulos[] = $elemento;
        }
    }
    return $nulos;
}

/**
 * Se usa para comprobar si una serie de valores son vacios
 * y devuelve un array de los vacios
 */
function empty_values_of_array($array) {
    $vacios = array();
    foreach ($array as $clave => $valor) {
        if (empty($valor)) {
            $vacios[] = $clave;
        }
    }
    return $vacios;
}

/**
 * Se usa para comprobar una expresion regular
 */
function match($expresion, $valor) {
    return preg_match($expresion, $valor);
}

/**
 * Se usa pasa comprobar si un año es bisiesto
 */
function leap_year($year = NULL) {
    $year = ($year == NULL) ? date('Y') : $year;
    return ( ($year % 4 == 0 && $year % 100 != 0) || $year % 400 == 0 );
}

/**
 * 
 * Se usa para devolver los nombres de las columnas concatenados
 * para hacer las consultas
 */
function introduced_columns_names($json, $bind = false) {
    $columnas = "";
    foreach ($json as $clave => $valor) {
        $columnas .= $bind ? ":bind_$clave, " : "$clave, ";
    }
    return substr($columnas, 0, -2); //devolver las columnas sin la ultima coma
}
