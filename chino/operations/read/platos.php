<?php

/**
 * Operación GET de recuperación de recursos mediante palabras
 * */
$app->get('/platos[/]', function ($request, $response, $args) {
    $atributos = array();

    // Crea la sentencia SQL añadiendo la condición por cada palabra buscada
    $sql_busqueda = ""
            . "SELECT * "
            . "FROM platos "
            . "WHERE codigo NOT LIKE '000' ORDER BY codigo";

    // Crear el objeto para poder operar
    $db = new DBModel();

    // Ejecutar la query
    $db->get_results_from_query($sql_busqueda);

    // Obtiene un array asociativo con los registros
    $records_busqueda = $db->get_rows();

    // Mostrar los errores
    // echo $db->get_errors();

    if ($records_busqueda != false) {
        $atributos["total"] = $db->get_row_count();
        $atributos["platos"] = $records_busqueda;
    } else {
        $atributos['error']['code'] = -1;
        $atributos['error']['message'] = "No se han encontrado platos";
    }

    // Convierte el array a formato JSON con caracteres Unicode y modo tabulado
    $atributos_json = json_encode($atributos, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

    return $response
                    ->withHeader('Content-type', 'application/json; charset=UTF-8')
                    ->write($atributos_json);
});
