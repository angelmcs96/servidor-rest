<?php

/**
 * Operación GET de recuperación de recursos mediante palabras
 * */
$app->get('/platos/nombre[/[{param_words}]]', function ($request, $response, $args) {
    $atributos = array();

    // Crea la sentencia SQL añadiendo la condición por cada palabra buscada
    $sql_busqueda = "SELECT * FROM platos";

    $conditions = array("codigo NOT LIKE '000'");

    // Filtrar por nombre si se le ha pasado el parametro
    if (!empty($args['param_words'])) {
        // Separa el parámetro de entrada en palabras
        $array_words = explode(' ', str_replace('+', ' ', $args['param_words']));

        if (sizeof($array_words) > 0) {
            foreach ($array_words as $clave => $valor) {
                // A la palabra se le añade el carácter '%' para la búsqueda
                $array_words[$clave] = "%$valor%";
                $conditions[] = "nombre LIKE ?";
            }
        }
    }

    // Convertir el array a string
    $conditions = join(' AND ', $conditions);

    // Añadir filtros
    $sql_busqueda .= " WHERE $conditions";

    //ordenar por fecha descendentemente
    $sql_busqueda .= " ORDER BY codigo";

    // echo $sql_busqueda;
    // Crear el objeto para poder operar
    $db = new DBModel();

    // Ejecutar la query
    $db->get_results_from_query($sql_busqueda, $array_words);

    // Mostrar los errores
    //echo $db->get_errors();
    // Obtiene un array asociativo con los registros
    $records_busqueda = $db->get_rows();

    if ($records_busqueda != false) {
        $atributos["total"] = $db->get_row_count();
        $atributos["platos"] = $records_busqueda;
    } else {
        $atributos['error']['code'] = -1;
        $atributos['error']['message'] = "No se han encontrado platos";
    }

    // Convierte el array a formato JSON con caracteres Unicode y modo tabulado
    $atributos_json = json_encode($atributos, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

    return $response
                    ->withHeader('Content-type', 'application/json; charset=UTF-8')
                    ->write($atributos_json);
});
