<?php

/**
 * Operación GET de recuperación de recursos mediante palabras
 * */
$app->get('/platos/order/{param_order}', function ($request, $response, $args) {
    $atributos = array();

    // Comprueba los parámetros
    if (empty($args['param_order'])) {
        $atributos['error']['code'] = -2;
        $atributos['error']['message'] = "Parámetros incorrectos en la petición";
    } else {
        $order = $args['param_order'];

        if ($order[0] == '-') {
            $order = substr($order, 1) . ' DESC';
        }

        // Crea la sentencia SQL añadiendo la condición por cada palabra buscada
        $sql_busqueda = ""
                . "SELECT * "
                . "FROM platos "
                . "WHERE codigo NOT LIKE '000' ORDER BY $order";

        // Crear el objeto para poder operar
        $db = new DBModel();

        // Ejecutar la query
        $db->get_results_from_query($sql_busqueda);

        // Obtiene un array asociativo con los registros
        $records_busqueda = $db->get_rows();

        if ($records_busqueda != false) {
            $atributos["total"] = $db->get_row_count();
            $atributos["platos"] = $records_busqueda;
        } else {
            $atributos['error']['code'] = -1;
            $atributos['error']['message'] = "No se han encontrado platos";
        }
    }

    // Convierte el array a formato JSON con caracteres Unicode y modo tabulado
    $atributos_json = json_encode($atributos, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

    return $response
                    ->withHeader('Content-type', 'application/json; charset=UTF-8')
                    ->write($atributos_json);
});
