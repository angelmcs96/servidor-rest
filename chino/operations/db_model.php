<?php

class DBModel {
    # Constantes

    CONST PRODUCTION = false;

    # Variables

    private $connection;
    private $errors;
    private $rows = array();
    private $row_count;

    # Definir datos de conexion

    private function db_data() {
        $data = array(
            'PRODUCTION' => array(
                'HOST' => 'mysql.hostinger.es',
                'NAME' => 'u446620630_chino',
                'USER' => 'u446620630_chino',
                'PASS' => 'poiuytr0'
            ),
            'DEVELOPMENT' => array(
                'HOST' => 'localhost',
                'NAME' => 'chino',
                'USER' => 'root',
                'PASS' => ''
            )
        );

        return self::PRODUCTION ? $data['PRODUCTION'] : $data ['DEVELOPMENT'];
    }

    # Conectar a la base de datos MySQL

    private function open_connection() {
        try {
            // Elimina los errores anteriores
            $this->reset_errors();

            // Datos de conexion
            $data = $this->db_data();

            $db_host = $data['HOST'];
            $db_name = $data['NAME'];
            $db_user = $data['USER'];
            $db_pass = $data['PASS'];

            $params = "mysql:host=$db_host;dbname=$db_name;charset=utf8";

            // Inicia conexión a la base de datos
            $pdo = new PDO($params, $db_user, $db_pass);

            if ($pdo != null) {
                // Activa las excepciones en el controlador PDO
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            }

            $this->connection = $pdo;
        } catch (Exception $e) {
            $this->errors = $e->getMessage();
        }
    }

    # Desconectar la base de datos

    private function close_connection() {
        $this->connection = null;
    }

    # Ejecuta una query y almacena el numero de filas

    private function execute_query($query, $args = array()) {
        try {
            // Abrir la conexion
            $this->open_connection();

            // Preparar y ejecutar la sentencia
            $stmt = $this->connection->prepare($query);
            $stmt->execute($args);

            // Almacena el numero de filas
            $this->set_row_count($stmt);

            return $stmt;
        } catch (Exception $e) {
            $this->errors = $e->getMessage();
        }
    }

    # Comprueba si el resultado de una consulta da alguna fila.

    public function exist_some_row($query, $args = array()) {
        try {
            // Ejecutar la sentencia
            $this->execute_query($query, $args);

            // Cierra la conexion
            $this->close_connection();

            return $this->row_count > 0;
        } catch (Exception $e) {
            $this->errors = $e->getMessage();
        }
    }

    # Ejecutar un query simple del tipo INSERT, DELETE, UPDATE

    public function execute_simple_query($query, $args = array()) {
        try {
            // Ejecutar la sentencia
            $this->execute_query($query, $args);

            // Cierra la conexion
            $this->close_connection();
        } catch (Exception $e) {
            $this->errors = $e->getMessage();
        }
    }

    # Almacenar resultados de una consulta en un Array

    public function get_results_from_query($query, $args = array()) {
        try {
            // Ejecutar la sentencia
            $stmt = $this->execute_query($query, $args);

            // Obtener un array asociativo con los registros
            $this->rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

            // Cierra la sentencia
            if ($stmt != null) {
                $stmt = null;
            }

            // Cierra la conexion
            $this->close_connection();
        } catch (Exception $e) {
            $this->errors = $e->getMessage();
        }
    }

    #Devuelve un array asociativo con las filas de la última consulta

    public function get_rows() {
        if (isset($this->rows)) {
            return $this->rows;
        }
        return null;
    }

    # Almacena el numero de filas encontradas

    private function set_row_count($statement) {
        $this->row_count = $statement->rowCount();
    }

    #Devuelve el número de filas de la última consulta realizada

    public function get_row_count() {
        if (isset($this->row_count)) {
            return $this->row_count;
        }
        return null;
    }

    # Reinicia los errores

    private function reset_errors() {
        unset($this->errors);
    }

    # Devuelve los errores que se hayan generado

    public function get_errors() {
        if (isset($this->errors) && !empty($this->errors)) {
            return $this->errors;
        }
        return 'No errors';
    }

}
