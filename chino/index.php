<?php

require 'vendor/autoload.php';

$app = new \Slim\App(['settings' => ['displayErrorDetails' => true]]);

/*
 * Incluimos la clase para gestionar la base de datos
 * y las funciones que se usaran al llamar a alguna URL
 */

include_once 'operations/db_model.php';
include_once 'operations/functions.php';

/*
 * Incluimos las rutas
 */

include_once 'operations/read/platos.php';
include_once 'operations/read/platos_order.php';
include_once 'operations/read/platos_nombre.php';

$app->run();
